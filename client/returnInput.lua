local testUI = CreateWebUI(0, 0, 0, 0, 1, 16)
SetWebAlignment(testUI, 0, 0)
SetWebAnchors(testUI, 0, 0, 1, 1)
SetWebURL(testUI, "http://asset/onsetuitest/client/ui.html")


Delay(1000, function()
  SetWebVisibility(testUI, WEB_VISIBLE)
  ShowMouseCursor(true)
  SetInputMode(INPUT_UI)
end)


AddEvent("OnTestDialogSubmit", function(first, second)
    SetWebVisibility(testUI, WEB_HIDDEN)
    ShowMouseCursor(false)
    SetInputMode(INPUT_GAME)
    AddPlayerChat("Data returned:")
    AddPlayerChat("Input box 1: "..first)
    AddPlayerChat("Input box 2: "..second)
end)
